#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
struct request
{
    int s;
    int f;
    int w;
};
typedef struct request request;

void swap(request* a, request * b)
{
    int temp[3];
    temp[0] = a->s;
    temp[1] = a->f;
    temp[2] = a->w;
    a->s = b->s;
    a->f = b->f;
    a->w = b->w;
    b->s = temp[0];
    b->f = temp[1];
    b->w = temp[2];
}

void copy(request* a, request * b)
{
    a->s = b->s;
    a->f = b->f;
    a->w = b->w;
}

void merge(request *a,int l,int m,int h)
{
	int n1=m-l+1;
	int n2=h-m;
	request left[n1+1],right[n2+1];
	int i,j,k;
	for(i=0;i<n1;i++)
		copy(&left[i],&a[l+i]);
	left[i].f=INT_MAX;
	for(i=0;i<n2;i++)
		copy(&right[i],&a[m+1+i]);
	right[i].f=INT_MAX;
	for(i=0,j=0,k=l;k<=h;k++)
	{
		if(left[i].f<=right[j].f)
		{
			copy(&a[k],&left[i]);
			i++;
		}
		else{
			copy(&a[k],&right[j]);
			j++;
		}
	}
}

void merge_sort(request *a,int l,int h)
{
	if(l<h)
	{
		int m=l+(h-l)/2;
		merge_sort(a,l,m);
		merge_sort(a,m+1,h);
		merge(a,l,m,h);
	}
}
int greater(int a, int b){return a>= b ? a : b;}

int firstcompatiblereq(request *a, int h)
{
    int l =0, key = a[h].s;
	while(l<=h)
	{
		int mid=l+(h-l)/2;
		if(a[mid].f<=key && a[mid+1].f>key)
			return mid;
		else if(a[mid].f>key)
			h=mid-1;
		else
			l=mid+1;
	}
	return -1;

}

int profititer(request *a, int n)
{
    int max[n];
    int i;
    for(i=0;i<n;i++)
        max[i]=0;
    max[0] = a[0].w;


    for(i=1;i<n;i++)
    {

        int incmax = a[i].w;
        int first = firstcompatiblereq(a,i);
        if(first != -1)
            incmax += max[first];

        max[i] = greater(incmax,max[i-1]);
    }
    int result = max[n-1];
    return result;
}
int maxrec[100000];
int memoized(request *a, int n)
{
    if(n==1)
    {
        maxrec[n] = a[n-1].w;
        return a[n-1].w;
    }
    int incmax = a[n-1].w;

    int i = firstcompatiblereq(a,n-1);
    if(i != -1)
    {
        if(maxrec[i+1] != -1)
            incmax += maxrec[i+1];
        else
            incmax += memoized(a, i+1);
    }

    int excmax;
    if(maxrec[n-1] != -1)
        excmax = maxrec[n-1];
    else
        excmax = memoized(a,n-1);
    int max = greater(incmax,excmax);
    maxrec[n] = max;
    return max;
}
int profitmemo(request *a, int n)
{
    int i;
    for(i=0;i<n;i++)
        maxrec[i] = -1;
    int max = memoized(a,n);
    return max;
}
int main()
{
	request a[] = {{1,3,3}, {4,11,9}, {9,12,5}, {5,8,2},{2,6,3},{7,10,8}};
    request b[] = {{8,10,2},{3,9,7},{6,7,4},{2,5,4},{1,4,2}};
    int na = sizeof(a)/sizeof(a[0]);
    int nb = sizeof(b)/sizeof(b[0]);
    merge_sort(a,0,na-1);
    merge_sort(b,0,nb-1);
    int maxa = profitmemo(a,na);
    printf("%d ",maxa);
    maxa = profititer(a,na);
    printf("%d\n",maxa);

    int maxb = profitmemo(b,nb);
    printf("%d ",maxb);
    maxa = profititer(b,nb);
    printf("%d\n",maxb);

    return 0;
}